# User Gateway API


<a name="overview"></a>
## Overview

### Version information
*Version* : v1




<a name="paths"></a>
## Paths

<a name="apiapplicationspost"></a>
### Create a new Application
```
POST /api/Applications
```


#### Description
For more information about Application, see documentation of Topology Service
            
Example of application:
            
    POST /api/Application
    {
        "applicationId": 0,
        "applicationName": "name",
        "resourceId": 0,
        "supportedOperations": [
            {
                "applicationOperationId": 0,
                "operationId": 0,
                "operationName": "name",
                "supportedRanges": [
                    {
                        "id": 0,
                        "name": "name",
                        "parameters": [
                            {
                                "id": 0,
                                "name": "name",
                                "minimum": 0,
                                "maximum": 0,
                                "minimumFunction": "{X}*2",
                                "maximumFunction": "{X}*4",
                                "defaultValue": 0
                            }
                        ]
                    }
                ]
            }
        ]
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**application**  <br>*optional*|The Application|object|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**201**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Applications


<a name="apiapplicationsget"></a>
### Get a shallow list of Applications
```
GET /api/Applications
```


#### Description
For more information about Application, see documentation of Topology Service
            
Example output:
            
    GET /api/Applications
    [
        {
            "applicationId": 0,
            "applicationName": "name",
            "resourceId": 0,
            "supportedOperations": null
        }
    ]


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* Applications


<a name="apiapplicationsbyidget"></a>
### Get a deep copy of an Application
```
GET /api/Applications/{id}
```


#### Description
For more information about Application, see documentation of Topology Service
            
Example output:
            
    GET /api/Application/{id}
    {
        "applicationId": 0,
        "applicationName": "name",
        "resourceId": 0,
        "supportedOperations": [
            {
                "applicationOperationId": 0,
                "operationId": 0,
                "operationName": "name",
                "supportedRanges": [
                    {
                        "id": 0,
                        "name": "name",
                        "parameters": [
                            {
                                "id": 0,
                                "name": "name",
                                "minimum": 0,
                                "maximum": 0,
                                "minimumFunction": "{X}*2",
                                "maximumFunction": "{X}*4",
                                "defaultValue": 0
                            }
                        ]
                    }
                ]
            }
        ]
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the Application to get|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* Applications


<a name="apiapplicationsbyidput"></a>
### Update an Application
```
PUT /api/Applications/{id}
```


#### Description
For more information about Application, see documentation of Topology Service
            
Example of application:
            
    PUT /api/Application/{id}
    {
        "applicationId": 0,
        "applicationName": "name",
        "resourceId": 0,
        "supportedOperations": [
            {
                "applicationOperationId": 0,
                "operationId": 0,
                "operationName": "name",
                "supportedRanges": [
                    {
                        "id": 0,
                        "name": "name",
                        "parameters": [
                            {
                                "id": 0,
                                "name": "name",
                                "minimum": 0,
                                "maximum": 0,
                                "minimumFunction": "{X}*2",
                                "maximumFunction": "{X}*4",
                                "defaultValue": 0
                            }
                        ]
                    }
                ]
            }
        ]
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|A valid Application ID|integer (int32)|
|**Body**|**application**  <br>*optional*|The updated Application|object|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Applications


<a name="apiapplicationsbyiddelete"></a>
### Delete an Application
```
DELETE /api/Applications/{id}
```


#### Description
For more information about Application, see documentation of Topology Service


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the Application to delete|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* Applications


<a name="apimoduleconveyorspost"></a>
### Create a module conveyor
```
POST /api/ModuleConveyors
```


#### Description
For more information about the ModuleConveyor model, see the Topology Service documentation
            
Example of module conveyor:
            
    POST /api/ModuleConveyors
    {
        "moduleConveyorId": 0,
        "moduleConveyorName": "unique-name",
        "plcIpAddressString": "10.0.0.144",
        "plcMacAddressString": "0011223344",
        "resourceId": 0
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**moduleConveyor**  <br>*optional*|The new module conveyor|object|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**201**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* ModuleConveyors


<a name="apimoduleconveyorsget"></a>
### Get all module conveyors
```
GET /api/ModuleConveyors
```


#### Description
For more information about the ModuleConveyor model, see the Topology Service documentation
            
Example output:
            
    GET /api/ModuleConveyors
    [
        {
            "moduleConveyorId": 0,
            "moduleConveyorName": "unique-name",
            "plcIpAddressString": "10.0.0.144",
            "plcMacAddressString": "0011223344",
            "resourceId": 0
        },
        {
            "moduleConveyorId": 1,
            "moduleConveyorName": "special-name",
            "plcIpAddressString": "10.0.0.145",
            "plcMacAddressString": "0011223355",
            "resourceId": 1
        }
    ]


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* ModuleConveyors


<a name="apimoduleconveyorsbyidget"></a>
### Get a module conveyor by ID
```
GET /api/ModuleConveyors/{id}
```


#### Description
For more information about the ModuleConveyor model, see the Topology Service documentation
            
Example output:
            
    GET /api/ModuleConveyors/{id}
    {
        "moduleConveyorId": 0,
        "moduleConveyorName": "unique-name",
        "plcIpAddressString": "10.0.0.144",
        "plcMacAddressString": "0011223344",
        "resourceId": 0
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the module conveyor|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* ModuleConveyors


<a name="apimoduleconveyorsbyidput"></a>
### Update a ModuleConveyor
```
PUT /api/ModuleConveyors/{id}
```


#### Description
For more information about the ModuleConveyor model, see the Topology Service documentation
            
Example of module conveyor:
            
    POST /api/ModuleConveyors/{id}
    {
        "moduleConveyorId": 0,
        "moduleConveyorName": "unique-name",
        "plcIpAddressString": "10.0.0.144",
        "plcMacAddressString": "0011223344",
        "resourceId": 0
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|A valid ID of the ModuleConveyor|integer (int32)|
|**Body**|**moduleConveyor**  <br>*optional*|The ModuleConveyor|object|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* ModuleConveyors


<a name="apimoduleconveyorsbyiddelete"></a>
### Delete a ModuleConveyor
```
DELETE /api/ModuleConveyors/{id}
```


#### Description
For more information about the ModuleConveyor model, see the Topology Service documentation
            
Example output:
            
    DELETE /api/ModuleConveyors/{id}
    {
        "moduleConveyorId": 0,
        "moduleConveyorName": "unique-name",
        "plcIpAddressString": "10.0.0.144",
        "plcMacAddressString": "0011223344",
        "resourceId": 0
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|A valid ID of the ModuleConveyor to delete|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* ModuleConveyors


<a name="apipingbypingmessageget"></a>
### Ping the service
```
GET /api/Ping/{pingMessage}
```


#### Description
This call can be used by other services to validate that User Gateway is still alive and running


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**pingMessage**  <br>*required*|An arbitraty string to return|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|string|


#### Produces

* `text/plain`
* `application/json`
* `text/json`


#### Tags

* Ping


<a name="apiresourcespost"></a>
### Create a Resource
```
POST /api/Resources
```


#### Description
For more information about the Resource model, see the Topology Service documentation
            
Example output:
            
    POST /api/Resources
    {
        "applicationId": 0,
        "moduleConveyorId": 0,
        "resourceId": 0,
        "resourceName": "Name"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**resource**  <br>*optional*|The Resource to create|object|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**201**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Resources


<a name="apiresourcesget"></a>
### Get a list of all Resources
```
GET /api/Resources
```


#### Description
For more information about the Resource model, see the Topology Service documentation
            
Example output:
            
    GET /api/Resources
    [
        {
            "applicationId": 0,
            "moduleConveyorId": 0,
            "resourceId": 0,
            "resourceName": "Name"
        },
        {
            "applicationId": 1,
            "moduleConveyorId": 2,
            "resourceId": 1,
            "resourceName": "Name"
        }
    ]


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* Resources


<a name="apiresourcesbyidget"></a>
### Get a Resource by ID
```
GET /api/Resources/{id}
```


#### Description
For more information about the Resource model, see the Topology Service documentation
            
Example output:
            
    GET /api/Resources/{id}
    {
        "applicationId": 0,
        "moduleConveyorId": 0,
        "resourceId": 0,
        "resourceName": "Name"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The Resource ID|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* Resources


<a name="apiresourcesbyidput"></a>
### Update a Resource
```
PUT /api/Resources/{id}
```


#### Description
For more information about the Resource model, see the Topology Service documentation
            
Example output:
            
    PUT /api/Resources/{id}
    {
        "applicationId": 0,
        "moduleConveyorId": 0,
        "resourceId": 0,
        "resourceName": "Name"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the Resource to update|integer (int32)|
|**Body**|**resource**  <br>*optional*|The updated Resource|object|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Resources


<a name="apiresourcesbyiddelete"></a>
### Delete a Resource
```
DELETE /api/Resources/{id}
```


#### Description
For more information about the Resource model, see the Topology Service documentation
            
Example output:
            
    DELETE /api/Resources/{id}
    {
        "applicationId": 0,
        "moduleConveyorId": 0,
        "resourceId": 0,
        "resourceName": "Name"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the Resource to delete|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* Resources


<a name="apiservicepost"></a>
### Announce a Service
```
POST /api/Service
```


#### Description
Example output:
            
    POST /api/Services
    {
        "uuid": "123e4567-e89b-12d3-a456-426655440000",
        "name": "name",
        "serviceType": 0,
        "isActive": true,
        "displayName": "Name",
        "serviceAddress": "http://localhost:5050/"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**service**  <br>*optional*|The Service to announce|[Service](#service)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**201**|Success|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Service


<a name="apiserviceget"></a>
### Get a list of all Services
```
GET /api/Service
```


#### Description
Example output:

    GET /api/Services
    [
        {
            "uuid": "123e4567-e89b-12d3-a456-426655440000",
            "name": "name",
            "serviceType": 0,
            "isActive": true,
            "displayName": "Name",
            "serviceAddress": "http://localhost:5050/"
        },
        {
            "uuid": "123e4567-e89b-12d3-a456-426655440001",
            "name": "name1",
            "serviceType": 1,
            "isActive": true,
            "displayName": "Name 1",
            "serviceAddress": "http://localhost:5051/"
        }
    ]


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Tags

* Service


<a name="apiservicenamebyservicenameget"></a>
### Get Service by name
```
GET /api/Service/name/{serviceName}
```


#### Description
Example output:
            
    GET /api/Services/name/{serviceName}
    {
        "uuid": "123e4567-e89b-12d3-a456-426655440000",
        "name": "serviceName",
        "serviceType": 0,
        "isActive": true,
        "displayName": "Name",
        "serviceAddress": "http://localhost:5050/"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**serviceName**  <br>*required*|The name of the requested Service|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**404**|Not Found|No Content|


#### Tags

* Service


<a name="apiservicetypebytypeidget"></a>
### Get the active Service by service type
```
GET /api/Service/type/{typeId}
```


#### Description
Example output:
            
    GET /api/Services/type/{typeId}
    {
        "uuid": "123e4567-e89b-12d3-a456-426655440000",
        "name": "name",
        "serviceType": 0,
        "isActive": true,
        "displayName": "Name",
        "serviceAddress": "http://localhost:5050/"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**typeId**  <br>*required*|The Service type|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**204**|No active service of type typeId|No Content|


#### Tags

* Service


<a name="apiservicebyuuidget"></a>
### Get a Service by UUID
```
GET /api/Service/{uuid}
```


#### Description
Example output:

    GET /api/Services/{uuid}
    {
        "uuid": "123e4567-e89b-12d3-a456-426655440000",
        "name": "name",
        "serviceType": 0,
        "isActive": true,
        "displayName": "Name",
        "serviceAddress": "http://localhost:5050/"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|The UUID of the requested Service|string (uuid)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**404**|Not Found|No Content|


#### Tags

* Service


<a name="apiservicebyuuidput"></a>
### Updated a Service
```
PUT /api/Service/{uuid}
```


#### Description
Example output:
            
    PUT /api/Services/{uuid}
    {
        "uuid": "123e4567-e89b-12d3-a456-426655440000",
        "name": "name",
        "serviceType": 0,
        "isActive": true,
        "displayName": "Name",
        "serviceAddress": "http://localhost:5050/"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|The UUID of the Service to update|string (uuid)|
|**Body**|**service**  <br>*optional*|The updated Service|[Service](#service)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Service


<a name="apiservicebyuuiddelete"></a>
### Delete a Service by UUID
```
DELETE /api/Service/{uuid}
```


#### Description
Example output:
            
    DELETE /api/Services/{uuid}
    {
        "uuid": "123e4567-e89b-12d3-a456-426655440000",
        "name": "name",
        "serviceType": 0,
        "isActive": true,
        "displayName": "Name",
        "serviceAddress": "http://localhost:5050/"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|The UUID of the Service to delete|string (uuid)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**404**|Not Found|No Content|


#### Tags

* Service


<a name="apitopologiespost"></a>
### Create Topology
```
POST /api/Topologies
```


#### Description
For more information on the Topology model, see documentation of Topology Service
            
Example Topology/output:
            
    POST api/Topologies
    {
        "topologyId": 0,
        "topologyName": "name",
        "isActive": true,
        "conveyorIds": [
            0, 1, 2, 3
        ],
        "conveyorLinkIds": {
            "0": {
                "0": 1
            },
            "1": {
                "0": 2
            },
            "2": {
                "0": 3
            },
            "3": {
                "0": 0
            }
        }
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**topologyJson**  <br>*optional*|The Topology|object|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**201**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Topologies


<a name="apitopologiesget"></a>
### Get a shallow list of Topologies
```
GET /api/Topologies
```


#### Description
For more information on the Topology model, see documentation of Topology Service
            
Example output:
            
    GET /api/Topologies
    [
        {
            "topologyId": 0,
            "topologyName": "name",
            "isActive": true,
            "conveyorIds": null,
            "conveyorLinkIds": null
        },
        {
            "topologyId": 1,
            "topologyName": "name2",
            "isActive": false,
            "conveyorIds": null,
            "conveyorLinkIds": null
        }
    ]


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* Topologies


<a name="apitopologiesbyidget"></a>
### Get a Topology by ID
```
GET /api/Topologies/{id}
```


#### Description
For more information on the Topology model, see documentation of Topology Service
            
Example output:
            
    GET api/Topologies/{id}
    {
        "topologyId": 0,
        "topologyName": "name",
        "isActive": true,
        "conveyorIds": [
            0, 1, 2, 3
        ],
        "conveyorLinkIds": {
            "0": {
                "0": 1
            },
            "1": {
                "0": 2
            },
            "2": {
                "0": 3
            },
            "3": {
                "0": 0
            }
        }
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|Topology ID|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* Topologies


<a name="apitopologiesbyidput"></a>
### Update Topology
```
PUT /api/Topologies/{id}
```


#### Description
For more information on the Topology model, see documentation of Topology Service
            
Example of Topology:
            
    PUT api/Topologies/{id}
    {
        "topologyId": 0,
        "topologyName": "name",
        "isActive": true,
        "conveyorIds": [
            0, 1, 2, 3
        ],
        "conveyorLinkIds": {
            "0": {
                "0": 1
            },
            "1": {
                "0": 2
            },
            "2": {
                "0": 3
            },
            "3": {
                "0": 0
            }
        }
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|ID of the Topology|integer (int32)|
|**Body**|**topologyJson**  <br>*optional*|The updated Topology|object|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Topologies


<a name="apitopologiesbyiddelete"></a>
### Delete Topology with ID
```
DELETE /api/Topologies/{id}
```


#### Description
For more information on the Topology model, see documentation of Topology Service
            
Example output:
            
    DELETE api/Topologies/{id}
    {
        "topologyId": 0,
        "topologyName": "name",
        "isActive": true,
        "conveyorIds": [
            0, 1, 2, 3
        ],
        "conveyorLinkIds": {
            "0": {
                "0": 1
            },
            "1": {
                "0": 2
            },
            "2": {
                "0": 3
            },
            "3": {
                "0": 0
            }
        }
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|ID of the Topology|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|
|**502**|Server Error|No Content|
|**503**|No Topology Service|No Content|


#### Tags

* Topologies




<a name="definitions"></a>
## Definitions

<a name="service"></a>
### Service

|Name|Schema|
|---|---|
|**displayName**  <br>*optional*|string|
|**isActive**  <br>*optional*|boolean|
|**name**  <br>*required*|string|
|**serviceAddress**  <br>*optional*|string|
|**serviceType**  <br>*optional*|integer (int32)|
|**uuid**  <br>*optional*|string (uuid)|





