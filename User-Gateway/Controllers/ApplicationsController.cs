﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Oxmes.Core.ServiceTypes;
using UserGateway.Controllers.Extensions;
using UserGateway.Exceptions;
using UserGateway.Interfaces;

namespace UserGateway.Controllers
{
    [Produces("application/json")]
    [Route("api/Applications")]
    public class ApplicationsController : Controller
    {
        private readonly IServiceRepository ServiceRepository;
        private readonly ILogger<ResourcesController> Logger;
        private HttpClient HttpClient;

        public ApplicationsController(IServiceRepository serviceRepository, ILogger<ResourcesController> logger)
        {
            ServiceRepository = serviceRepository;
            Logger = logger;
        }

        private bool TryCreateHttpClient()
        {
            try
            {
                var topologyService = ServiceRepository.FindActiveByTypeId((int)ServiceType.Topology);

                if (topologyService.ServiceAddress != null)
                {
                    HttpClient = new HttpClient() { BaseAddress = new Uri(topologyService.ServiceAddress + "/api/applications/") };
                    return true;
                }

                return false;
            }
            catch (ServiceNotFoundException)
            {
                return false;
            }
        }

        private bool TryGetHttpClient(out HttpClient httpClient)
        {
            if (HttpClient == null)
            {
                var result = TryCreateHttpClient();
                httpClient = HttpClient;
                return result;
            }
            else
            {
                httpClient = HttpClient;
                return true;
            }
        }

        private async Task<IActionResult> HandleRequest(Func<HttpClient, Task<IActionResult>> logic)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    return await logic(httpClient);
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Get a shallow list of Applications
        /// </summary>
        /// <returns>A shallow list of Applications</returns>
        /// <remarks>
        /// For more information about Application, see documentation of Topology Service
        ///
        /// Example output:
        ///
        ///     GET /api/Applications
        ///     [
        ///         {
        ///             "applicationId": 0,
        ///             "applicationName": "name",
        ///             "resourceId": 0,
        ///             "supportedOperations": null
        ///         }
        ///     ]
        ///
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // GET: api/Application
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> GetApplications()
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    using (var response = await httpClient.GetAsync(""))
                    {
                        return this.OkJson(await response.Content.ReadAsStringAsync());
                    }
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Get a deep copy of an Application
        /// </summary>
        /// <param name="id">The ID of the Application to get</param>
        /// <remarks>
        /// For more information about Application, see documentation of Topology Service
        ///
        /// Example output:
        ///
        ///     GET /api/Application/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "applicationName": "name",
        ///         "resourceId": 0,
        ///         "supportedOperations": [
        ///             {
        ///                 "applicationOperationId": 0,
        ///                 "operationId": 0,
        ///                 "operationName": "name",
        ///                 "supportedRanges": [
        ///                     {
        ///                         "id": 0,
        ///                         "name": "name",
        ///                         "parameters": [
        ///                             {
        ///                                 "id": 0,
        ///                                 "name": "name",
        ///                                 "minimum": 0,
        ///                                 "maximum": 0,
        ///                                 "minimumFunction": "{X}*2",
        ///                                 "maximumFunction": "{X}*4",
        ///                                 "defaultValue": 0
        ///                             }
        ///                         ]
        ///                     }
        ///                 ]
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // GET: api/Application/5
        [HttpGet("{id}", Name = "GetApplication")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> GetApplication(int id)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    using (var response = await httpClient.GetAsync($"{id}"))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            return this.OkJson(await response.Content.ReadAsStringAsync());
                        }
                        else return StatusCode((int)response.StatusCode);
                    }
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Create a new Application
        /// </summary>
        /// <param name="application">The Application</param>
        /// <returns>The Application with an ID</returns>
        /// <remarks>
        /// For more information about Application, see documentation of Topology Service
        ///
        /// Example of application:
        ///
        ///     POST /api/Application
        ///     {
        ///         "applicationId": 0,
        ///         "applicationName": "name",
        ///         "resourceId": 0,
        ///         "supportedOperations": [
        ///             {
        ///                 "applicationOperationId": 0,
        ///                 "operationId": 0,
        ///                 "operationName": "name",
        ///                 "supportedRanges": [
        ///                     {
        ///                         "id": 0,
        ///                         "name": "name",
        ///                         "parameters": [
        ///                             {
        ///                                 "id": 0,
        ///                                 "name": "name",
        ///                                 "minimum": 0,
        ///                                 "maximum": 0,
        ///                                 "minimumFunction": "{X}*2",
        ///                                 "maximumFunction": "{X}*4",
        ///                                 "defaultValue": 0
        ///                             }
        ///                         ]
        ///                     }
        ///                 ]
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // POST: api/Application
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> Post([FromBody]JObject application)
        {
            return await HandleRequest(async (httpClient) =>
            {
                var content = new StringContent(application.ToString(Newtonsoft.Json.Formatting.None), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("", content))
                {
                    return await this.CreatedAtActionOrFailedAsync(response, "GetApplication", "applicationId");
                }
            });
        }

        /// <summary>
        /// Update an Application
        /// </summary>
        /// <param name="id">A valid Application ID</param>
        /// <param name="application">The updated Application</param>
        /// <remarks>
        /// For more information about Application, see documentation of Topology Service
        ///
        /// Example of application:
        ///
        ///     PUT /api/Application/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "applicationName": "name",
        ///         "resourceId": 0,
        ///         "supportedOperations": [
        ///             {
        ///                 "applicationOperationId": 0,
        ///                 "operationId": 0,
        ///                 "operationName": "name",
        ///                 "supportedRanges": [
        ///                     {
        ///                         "id": 0,
        ///                         "name": "name",
        ///                         "parameters": [
        ///                             {
        ///                                 "id": 0,
        ///                                 "name": "name",
        ///                                 "minimum": 0,
        ///                                 "maximum": 0,
        ///                                 "minimumFunction": "{X}*2",
        ///                                 "maximumFunction": "{X}*4",
        ///                                 "defaultValue": 0
        ///                             }
        ///                         ]
        ///                     }
        ///                 ]
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // PUT: api/Application/5
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> Put(int id, [FromBody]JObject application)
        {
            return await HandleRequest(async (httpClient) =>
            {
                var content = new StringContent(application.ToString(Newtonsoft.Json.Formatting.None), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync($"{id}", content))
                {
                    return StatusCode((int)response.StatusCode);
                }
            });
        }

        /// <summary>
        /// Delete an Application
        /// </summary>
        /// <param name="id">The ID of the Application to delete</param>
        /// <returns>The deleted Application</returns>
        /// <remarks>
        /// For more information about Application, see documentation of Topology Service
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // DELETE: api/Application/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> Delete(int id)
        {
            return await HandleRequest(async (httpClient) =>
            {
                using (var response = await httpClient.DeleteAsync($"{id}"))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        return this.OkJson(await response.Content.ReadAsStringAsync());
                    }
                    else return StatusCode((int)response.StatusCode);
                }
            });
        }
    }
}