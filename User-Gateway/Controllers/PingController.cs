﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UserGateway.Controllers
{
    [Route("api/Ping")]
    public class PingController : Controller
    {
        /// <summary>
        /// Ping the service
        /// </summary>
        /// <param name="pingMessage">An arbitraty string to return</param>
        /// <returns>The value of <paramref name="pingMessage"/></returns>
        /// <remarks>
        /// This call can be used by other services to validate that User Gateway is still alive and running
        /// </remarks>
        /// <response code="200">Success</response>
        [HttpGet("{pingMessage}")]
        public string Ping([FromRoute] string pingMessage)
        {
            return pingMessage;
        }
    }
}