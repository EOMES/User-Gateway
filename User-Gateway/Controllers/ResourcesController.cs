﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Oxmes.Core.ServiceTypes;
using UserGateway.Controllers.Extensions;
using UserGateway.Exceptions;
using UserGateway.Interfaces;

namespace UserGateway.Controllers
{
    [Produces("application/json")]
    [Route("api/Resources")]
    public class ResourcesController : Controller
    {
        private readonly IServiceRepository ServiceRepository;
        private readonly ILogger<ResourcesController> Logger;
        private HttpClient HttpClient;

        public ResourcesController(IServiceRepository serviceRepository, ILogger<ResourcesController> logger)
        {
            ServiceRepository = serviceRepository;
            Logger = logger;
        }

        private bool TryCreateHttpClient()
        {
            try
            {
                var topologyService = ServiceRepository.FindActiveByTypeId((int)ServiceType.Topology);

                if (topologyService.ServiceAddress != null)
                {
                    HttpClient = new HttpClient() { BaseAddress = new Uri(topologyService.ServiceAddress + "/api/resources/") };
                    return true;
                }

                return false;
            }
            catch (ServiceNotFoundException)
            {
                return false;
            }
        }

        private bool TryGetHttpClient(out HttpClient httpClient)
        {
            if (HttpClient == null)
            {
                var result = TryCreateHttpClient();
                httpClient = HttpClient;
                return result;
            }
            else
            {
                httpClient = HttpClient;
                return true;
            }
        }

        private async Task<IActionResult> HandleRequest(Func<HttpClient, Task<IActionResult>> logic)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    return await logic(httpClient);
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Get a list of all Resources
        /// </summary>
        /// <returns>A list of all Resources</returns>
        /// <remarks>
        /// For more information about the Resource model, see the Topology Service documentation
        ///
        /// Example output:
        ///
        ///     GET /api/Resources
        ///     [
        ///         {
        ///             "applicationId": 0,
        ///             "moduleConveyorId": 0,
        ///             "resourceId": 0,
        ///             "resourceName": "Name"
        ///         },
        ///         {
        ///             "applicationId": 1,
        ///             "moduleConveyorId": 2,
        ///             "resourceId": 1,
        ///             "resourceName": "Name"
        ///         }
        ///     ]
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // GET: api/Resourses
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> GetResources()
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    using (var response = await httpClient.GetAsync(""))
                    {
                        return this.OkJson(await response.Content.ReadAsStringAsync());
                    }
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Get a Resource by ID
        /// </summary>
        /// <param name="id">The Resource ID</param>
        /// <returns>A Resource</returns>
        /// <remarks>
        /// For more information about the Resource model, see the Topology Service documentation
        ///
        /// Example output:
        ///
        ///     GET /api/Resources/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "moduleConveyorId": 0,
        ///         "resourceId": 0,
        ///         "resourceName": "Name"
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // GET: api/Resourses/5
        [HttpGet("{id}", Name = "GetResource")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> GetResource(int id)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    using (var response = await httpClient.GetAsync($"{id}"))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            return this.OkJson(await response.Content.ReadAsStringAsync());
                        }
                        else return StatusCode((int)response.StatusCode);
                    }
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Create a Resource
        /// </summary>
        /// <param name="resource">The Resource to create</param>
        /// <returns>The created Resource with its ID</returns>
        /// <remarks>
        /// For more information about the Resource model, see the Topology Service documentation
        ///
        /// Example output:
        ///
        ///     POST /api/Resources
        ///     {
        ///         "applicationId": 0,
        ///         "moduleConveyorId": 0,
        ///         "resourceId": 0,
        ///         "resourceName": "Name"
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // POST: api/Resourses
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> Post([FromBody]JObject resource)
        {
            return await HandleRequest(async (httpClient) =>
            {
                var content = new StringContent(resource.ToString(Newtonsoft.Json.Formatting.None), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("", content))
                {
                    return await this.CreatedAtActionOrFailedAsync(response, "GetResource", "resourceId");
                }
            });
        }

        /// <summary>
        /// Update a Resource
        /// </summary>
        /// <param name="id">The ID of the Resource to update</param>
        /// <param name="resource">The updated Resource</param>
        /// <remarks>
        /// For more information about the Resource model, see the Topology Service documentation
        ///
        /// Example output:
        ///
        ///     PUT /api/Resources/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "moduleConveyorId": 0,
        ///         "resourceId": 0,
        ///         "resourceName": "Name"
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // PUT: api/Resourses/5
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> Put(int id, [FromBody]JObject resource)
        {
            return await HandleRequest(async (httpClient) =>
            {
                var content = new StringContent(resource.ToString(Newtonsoft.Json.Formatting.None), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync($"{id}", content))
                {
                    return StatusCode((int)response.StatusCode);
                }
            });
        }

        /// <summary>
        /// Delete a Resource
        /// </summary>
        /// <param name="id">The ID of the Resource to delete</param>
        /// <returns>The deleted Resource</returns>
        /// <remarks>
        /// For more information about the Resource model, see the Topology Service documentation
        ///
        /// Example output:
        ///
        ///     DELETE /api/Resources/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "moduleConveyorId": 0,
        ///         "resourceId": 0,
        ///         "resourceName": "Name"
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // DELETE: api/Resourses/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> Delete(int id)
        {
            return await HandleRequest(async (httpClient) =>
            {
                using (var response = await httpClient.DeleteAsync($"{id}"))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        return this.OkJson(await response.Content.ReadAsStringAsync());
                    }
                    else return StatusCode((int)response.StatusCode);
                }
            });
        }
    }
}