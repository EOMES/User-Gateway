﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Oxmes.Core.ServiceTypes;
using UserGateway.Controllers.Extensions;
using UserGateway.Exceptions;
using UserGateway.Interfaces;

namespace UserGateway.Controllers
{
    [Produces("application/json")]
    [Route("api/Topologies")]
    public class TopologiesController : Controller
    {
        private readonly IServiceRepository ServiceRepository;
        private readonly ILogger<TopologiesController> Logger;

        private HttpClient HttpClient { get; set; }

        public TopologiesController(IServiceRepository serviceRepository, ILogger<TopologiesController> logger)
        {
            ServiceRepository = serviceRepository;
            Logger = logger;
        }

        private bool TryCreateHttpClient()
        {
            try
            {
                var topologyService = ServiceRepository.FindActiveByTypeId((int)ServiceType.Topology);

                if (topologyService.ServiceAddress != null)
                {
                    HttpClient = new HttpClient() { BaseAddress = new Uri(topologyService.ServiceAddress + "/api/topologies") };
                    return true;
                }

                return false;
            }
            catch (ServiceNotFoundException)
            {
                return false;
            }
        }

        private bool TryGetHttpClient(out HttpClient httpClient)
        {
            if (HttpClient == null)
            {
                var result = TryCreateHttpClient();
                httpClient = HttpClient;
                return result;
            }
            else
            {
                httpClient = HttpClient;
                return true;
            }
        }

        /// <summary>
        /// Get a shallow list of Topologies
        /// </summary>
        /// <returns>A shallow list of Topologies</returns>
        /// <remarks>
        /// For more information on the Topology model, see documentation of Topology Service
        ///
        /// Example output:
        ///
        ///     GET /api/Topologies
        ///     [
        ///         {
        ///             "topologyId": 0,
        ///             "topologyName": "name",
        ///             "isActive": true,
        ///             "conveyorIds": null,
        ///             "conveyorLinkIds": null
        ///         },
        ///         {
        ///             "topologyId": 1,
        ///             "topologyName": "name2",
        ///             "isActive": false,
        ///             "conveyorIds": null,
        ///             "conveyorLinkIds": null
        ///         }
        ///     ]
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> GetTopologies()
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    using (var response = await httpClient.GetAsync(""))
                    {
                        return await this.OkOrFailed(response);
                    }
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Get a Topology by ID
        /// </summary>
        /// <param name="id">Topology ID</param>
        /// <returns>The Topology with all conveyors and conveyorLinks</returns>
        /// <remarks>
        /// For more information on the Topology model, see documentation of Topology Service
        ///
        /// Example output:
        ///
        ///     GET api/Topologies/{<paramref name="id"/>}
        ///     {
        ///         "topologyId": 0,
        ///         "topologyName": "name",
        ///         "isActive": true,
        ///         "conveyorIds": [
        ///             0, 1, 2, 3
        ///         ],
        ///         "conveyorLinkIds": {
        ///             "0": {
        ///                 "0": 1
        ///             },
        ///             "1": {
        ///                 "0": 2
        ///             },
        ///             "2": {
        ///                 "0": 3
        ///             },
        ///             "3": {
        ///                 "0": 0
        ///             }
        ///         }
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> GetTopology([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    using (var response = await httpClient.GetAsync($"topologies/{id}"))
                    {
                        return await this.OkOrFailed(response);
                    }
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Update Topology
        /// </summary>
        /// <param name="id">ID of the Topology</param>
        /// <param name="topologyJson">The updated Topology</param>
        /// <remarks>
        /// For more information on the Topology model, see documentation of Topology Service
        ///
        /// Example of Topology:
        ///
        ///     PUT api/Topologies/{<paramref name="id"/>}
        ///     {
        ///         "topologyId": 0,
        ///         "topologyName": "name",
        ///         "isActive": true,
        ///         "conveyorIds": [
        ///             0, 1, 2, 3
        ///         ],
        ///         "conveyorLinkIds": {
        ///             "0": {
        ///                 "0": 1
        ///             },
        ///             "1": {
        ///                 "0": 2
        ///             },
        ///             "2": {
        ///                 "0": 3
        ///             },
        ///             "3": {
        ///                 "0": 0
        ///             }
        ///         }
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> UpdateTopology([FromRoute] int id, [FromBody] JObject topologyJson)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    var content = new StringContent(topologyJson.ToString(Newtonsoft.Json.Formatting.None), Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PutAsync($"topologies/{id}", content))
                    {
                        return StatusCode((int)response.StatusCode);
                    }
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Create Topology
        /// </summary>
        /// <param name="topologyJson">The Topology</param>
        /// <returns>The Topology with a ID</returns>
        /// <remarks>
        /// For more information on the Topology model, see documentation of Topology Service
        ///
        /// Example Topology/output:
        ///
        ///     POST api/Topologies
        ///     {
        ///         "topologyId": 0,
        ///         "topologyName": "name",
        ///         "isActive": true,
        ///         "conveyorIds": [
        ///             0, 1, 2, 3
        ///         ],
        ///         "conveyorLinkIds": {
        ///             "0": {
        ///                 "0": 1
        ///             },
        ///             "1": {
        ///                 "0": 2
        ///             },
        ///             "2": {
        ///                 "0": 3
        ///             },
        ///             "3": {
        ///                 "0": 0
        ///             }
        ///         }
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> PostTopology([FromBody] JObject topologyJson)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    var content = new StringContent(topologyJson.ToString(Newtonsoft.Json.Formatting.None), Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync("", content))
                    {
                        return await this.CreatedAtActionOrFailedAsync(response, "GetTopology", "topologyId");
                    }
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Delete Topology with ID
        /// </summary>
        /// <param name="id">ID of the Topology</param>
        /// <returns>The deleted Topology</returns>
        /// <remarks>
        /// For more information on the Topology model, see documentation of Topology Service
        ///
        /// Example output:
        ///
        ///     DELETE api/Topologies/{<paramref name="id"/>}
        ///     {
        ///         "topologyId": 0,
        ///         "topologyName": "name",
        ///         "isActive": true,
        ///         "conveyorIds": [
        ///             0, 1, 2, 3
        ///         ],
        ///         "conveyorLinkIds": {
        ///             "0": {
        ///                 "0": 1
        ///             },
        ///             "1": {
        ///                 "0": 2
        ///             },
        ///             "2": {
        ///                 "0": 3
        ///             },
        ///             "3": {
        ///                 "0": 0
        ///             }
        ///         }
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> DeleteTopology([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    using (var response = await httpClient.DeleteAsync($"topologies/{id}"))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            return this.OkJson(await response.Content.ReadAsStringAsync());
                        }
                        else return StatusCode((int)response.StatusCode);
                    }
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }
    }
}