﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserGateway.Interfaces;
using UserGateway.Models;

namespace UserGateway.Controllers
{
    [Produces("application/json")]
    [Route("api/Service")]
    public class ServiceController : Controller
    {
        private readonly IServiceRepository ServiceRepository;

        public ServiceController(IServiceRepository serviceRepository)
        {
            ServiceRepository = serviceRepository;
        }

        /// <summary>
        /// Get a list of all Services
        /// </summary>
        /// <returns>A list of all Services</returns>
        /// <remarks>
        /// Example output:
        /// 
        ///     GET /api/Services
        ///     [
        ///         {
        ///             "uuid": "123e4567-e89b-12d3-a456-426655440000",
        ///             "name": "name",
        ///             "serviceType": 0,
        ///             "isActive": true,
        ///             "displayName": "Name",
        ///             "serviceAddress": "http://localhost:5050/"
        ///         },
        ///         {
        ///             "uuid": "123e4567-e89b-12d3-a456-426655440001",
        ///             "name": "name1",
        ///             "serviceType": 1,
        ///             "isActive": true,
        ///             "displayName": "Name 1",
        ///             "serviceAddress": "http://localhost:5051/"
        ///         }
        ///     ]
        /// </remarks>
        // GET: api/Service
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Get()
        {
            return Ok(ServiceRepository.All);
        }

        /// <summary>
        /// Get a Service by UUID
        /// </summary>
        /// <param name="uuid">The UUID of the requested Service</param>
        /// <returns>The requested Service</returns>
        /// <remarks>
        /// Example output:
        /// 
        ///     GET /api/Services/{<paramref name="uuid"/>}
        ///     {
        ///         "uuid": "123e4567-e89b-12d3-a456-426655440000",
        ///         "name": "name",
        ///         "serviceType": 0,
        ///         "isActive": true,
        ///         "displayName": "Name",
        ///         "serviceAddress": "http://localhost:5050/"
        ///     }
        /// </remarks>
        // GET: api/Service/[uuid]
        [HttpGet("{uuid}", Name = "GetUUID")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Get(Guid uuid)
        {
            if (ServiceRepository.ServiceExists(uuid))
            {
                return Ok(ServiceRepository.FindByUuid(uuid));
            }
            else return NotFound();
        }

        /// <summary>
        /// Get the active Service by service type
        /// </summary>
        /// <param name="typeId">The Service type</param>
        /// <returns>The active Service of type <paramref name="typeId"/></returns>
        /// <remarks>
        /// Example output:
        ///
        ///     GET /api/Services/type/{<paramref name="typeId"/>}
        ///     {
        ///         "uuid": "123e4567-e89b-12d3-a456-426655440000",
        ///         "name": "name",
        ///         "serviceType": 0,
        ///         "isActive": true,
        ///         "displayName": "Name",
        ///         "serviceAddress": "http://localhost:5050/"
        ///     }
        /// </remarks>
        /// <response code="204">No active service of type <paramref name="typeId"/></response>
        // GET: api/Service/type/5
        [HttpGet("type/{typeId}", Name = "GetTypeId")]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        public IActionResult Get(int typeId)
        {
            if (ServiceRepository.ServiceTypeExists(typeId))
            {
                return Ok(ServiceRepository.FindActiveByTypeId(typeId));
            }
            else return NoContent();
        }

        /// <summary>
        /// Get Service by name
        /// </summary>
        /// <param name="serviceName">The name of the requested Service</param>
        /// <returns>The requested Service</returns>
        /// <remarks>
        /// Example output:
        ///
        ///     GET /api/Services/name/{<paramref name="serviceName"/>}
        ///     {
        ///         "uuid": "123e4567-e89b-12d3-a456-426655440000",
        ///         "name": "serviceName",
        ///         "serviceType": 0,
        ///         "isActive": true,
        ///         "displayName": "Name",
        ///         "serviceAddress": "http://localhost:5050/"
        ///     }
        /// </remarks>
        // GET: api/Service/name/[serviceName]
        [HttpGet("name/{serviceName}", Name = "GetName")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Get(string serviceName)
        {
            if (ServiceRepository.ServiceExists(serviceName))
            {
                return Ok(ServiceRepository.FindByName(serviceName));
            }
            else return NotFound();
        }

        /// <summary>
        /// Announce a Service
        /// </summary>
        /// <param name="service">The Service to announce</param>
        /// <returns>The Service with a given UUID</returns>
        /// <remarks>
        /// Example output:
        ///
        ///     POST /api/Services
        ///     {
        ///         "uuid": "123e4567-e89b-12d3-a456-426655440000",
        ///         "name": "name",
        ///         "serviceType": 0,
        ///         "isActive": true,
        ///         "displayName": "Name",
        ///         "serviceAddress": "http://localhost:5050/"
        ///     }
        /// </remarks>
        // POST: api/Service
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(201)]
        public IActionResult Post([FromBody] Service service)
        {
            if (!ModelState.IsValid || service == null) return BadRequest();

            service.UUID = Guid.NewGuid();
            ServiceRepository.Add(service);
            return CreatedAtRoute(service.UUID, service);
        }

        /// <summary>
        /// Updated a Service
        /// </summary>
        /// <param name="uuid">The UUID of the Service to update</param>
        /// <param name="service">The updated Service</param>
        /// <remarks>
        /// Example output:
        ///
        ///     PUT /api/Services/{<paramref name="uuid"/>}
        ///     {
        ///         "uuid": "123e4567-e89b-12d3-a456-426655440000",
        ///         "name": "name",
        ///         "serviceType": 0,
        ///         "isActive": true,
        ///         "displayName": "Name",
        ///         "serviceAddress": "http://localhost:5050/"
        ///     }
        /// </remarks>
        // PUT: api/Service/[uuid]
        [HttpPut("{uuid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Put(Guid uuid, [FromBody]Service service)
        {
            if (!ModelState.IsValid || uuid == null || service == null) return BadRequest();

            if (ServiceRepository.ServiceExists(uuid))
            {
                ServiceRepository.Remove(ServiceRepository.FindByUuid(uuid));
                ServiceRepository.Add(service);
                return Ok();
            }
            else return NotFound();
        }

        /// <summary>
        /// Delete a Service by UUID
        /// </summary>
        /// <param name="uuid">The UUID of the Service to delete</param>
        /// <remarks>
        /// Example output:
        ///
        ///     DELETE /api/Services/{<paramref name="uuid"/>}
        ///     {
        ///         "uuid": "123e4567-e89b-12d3-a456-426655440000",
        ///         "name": "name",
        ///         "serviceType": 0,
        ///         "isActive": true,
        ///         "displayName": "Name",
        ///         "serviceAddress": "http://localhost:5050/"
        ///     }
        /// </remarks>
        // DELETE: api/Service/[uuid]
        [HttpDelete("{uuid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Delete(Guid uuid)
        {
            if (!ModelState.IsValid || uuid == null) return BadRequest();

            if (ServiceRepository.ServiceExists(uuid))
            {
                ServiceRepository.Remove(ServiceRepository.FindByUuid(uuid));
                return Ok();
            }
            else return NotFound();
        }
    }
}