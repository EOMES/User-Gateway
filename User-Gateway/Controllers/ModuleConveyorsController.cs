﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Oxmes.Core.ServiceTypes;
using UserGateway.Controllers.Extensions;
using UserGateway.Exceptions;
using UserGateway.Interfaces;

namespace UserGateway.Controllers
{
    [Produces("application/json")]
    [Route("api/ModuleConveyors")]
    public class ModuleConveyorsController : Controller
    {
        private readonly IServiceRepository ServiceRepository;
        private readonly ILogger Logger;

        private HttpClient HttpClient { get; set; }

        public ModuleConveyorsController(IServiceRepository serviceRepository, ILogger<ModuleConveyorsController> logger)
        {
            ServiceRepository = serviceRepository;
            Logger = logger;
        }

        private bool TryCreateHttpClient()
        {
            try
            {
                var topologyService = ServiceRepository.FindActiveByTypeId((int)ServiceType.Topology);

                if (topologyService.ServiceAddress != null)
                {
                    HttpClient = new HttpClient() { BaseAddress = new Uri(topologyService.ServiceAddress + "/api/ModuleConveyors/") };
                    return true;
                }

                return false;
            }
            catch (ServiceNotFoundException)
            {
                return false;
            }
        }

        private bool TryGetHttpClient(out HttpClient httpClient)
        {
            if (HttpClient == null)
            {
                var result = TryCreateHttpClient();
                httpClient = HttpClient;
                return result;
            }
            else
            {
                httpClient = HttpClient;
                return true;
            }
        }

        private async Task<IActionResult> HandleRequest(Func<HttpClient, Task<IActionResult>> logic)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (TryGetHttpClient(out var httpClient))
            {
                try
                {
                    return await logic(httpClient);
                }
                catch (HttpRequestException ex)
                {
                    Logger.LogError(ex, "Unable to service request");
                    return StatusCode(502);
                }
            }
            else return StatusCode(503);
        }

        /// <summary>
        /// Get all module conveyors
        /// </summary>
        /// <returns>A list of all module conveyors</returns>
        /// <remarks>
        /// For more information about the ModuleConveyor model, see the Topology Service documentation
        ///
        /// Example output:
        ///
        ///     GET /api/ModuleConveyors
        ///     [
        ///         {
        ///             "moduleConveyorId": 0,
        ///             "moduleConveyorName": "unique-name",
        ///             "plcIpAddressString": "10.0.0.144",
        ///             "plcMacAddressString": "0011223344",
        ///             "resourceId": 0
        ///         },
        ///         {
        ///             "moduleConveyorId": 1,
        ///             "moduleConveyorName": "special-name",
        ///             "plcIpAddressString": "10.0.0.145",
        ///             "plcMacAddressString": "0011223355",
        ///             "resourceId": 1
        ///         }
        ///     ]
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // GET: api/ModuleConveyors
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> GetConveyors()
        {
            return await HandleRequest(async (httpClient) =>
            {
                using (var response = await httpClient.GetAsync(""))
                {
                    return this.OkJson(await response.Content.ReadAsStringAsync());
                }
            });
        }

        /// <summary>
        /// Get a module conveyor by ID
        /// </summary>
        /// <param name="id">The ID of the module conveyor</param>
        /// <returns>A module conveyor</returns>
        /// <remarks>
        /// For more information about the ModuleConveyor model, see the Topology Service documentation
        ///
        /// Example output:
        ///
        ///     GET /api/ModuleConveyors/{<paramref name="id"/>}
        ///     {
        ///         "moduleConveyorId": 0,
        ///         "moduleConveyorName": "unique-name",
        ///         "plcIpAddressString": "10.0.0.144",
        ///         "plcMacAddressString": "0011223344",
        ///         "resourceId": 0
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // GET: api/ModuleConveyors/5
        [HttpGet("{id}", Name = "GetModuleConveyor")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> GetModuleConveyor([FromRoute] int? id)
        {
            return await HandleRequest(async (httpClient) =>
            {
                using (var response = await httpClient.GetAsync($"{id}"))
                {
                    return this.OkJson(await response.Content.ReadAsStringAsync());
                }
            });
        }

        /// <summary>
        /// Create a module conveyor
        /// </summary>
        /// <param name="moduleConveyor">The new module conveyor</param>
        /// <returns>The ModuleConveyor with an ID</returns>
        /// <remarks>
        /// For more information about the ModuleConveyor model, see the Topology Service documentation
        ///
        /// Example of module conveyor:
        ///
        ///     POST /api/ModuleConveyors
        ///     {
        ///         "moduleConveyorId": 0,
        ///         "moduleConveyorName": "unique-name",
        ///         "plcIpAddressString": "10.0.0.144",
        ///         "plcMacAddressString": "0011223344",
        ///         "resourceId": 0
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // POST: api/ModuleConveyors
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> PostModuleConveyor([FromBody] JObject moduleConveyor)
        {
            return await HandleRequest(async (httpClient) =>
            {
                var content = new StringContent(moduleConveyor.ToString(Newtonsoft.Json.Formatting.None), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync("", content))
                {
                    return await this.CreatedAtActionOrFailedAsync(response, "GetModuleConveyor", "moduleConveyorId");
                }
            });
        }

        /// <summary>
        /// Update a ModuleConveyor
        /// </summary>
        /// <param name="id">A valid ID of the ModuleConveyor</param>
        /// <param name="moduleConveyor">The ModuleConveyor</param>
        /// <remarks>
        /// For more information about the ModuleConveyor model, see the Topology Service documentation
        ///
        /// Example of module conveyor:
        ///
        ///     POST /api/ModuleConveyors/{<paramref name="id"/>}
        ///     {
        ///         "moduleConveyorId": 0,
        ///         "moduleConveyorName": "unique-name",
        ///         "plcIpAddressString": "10.0.0.144",
        ///         "plcMacAddressString": "0011223344",
        ///         "resourceId": 0
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> PutModuleConveyor([FromRoute] int? id, [FromBody] JObject moduleConveyor)
        {
            return await HandleRequest(async (httpClient) =>
            {
                var content = new StringContent(moduleConveyor.ToString(Newtonsoft.Json.Formatting.None), Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync($"{id}", content))
                {
                    return StatusCode((int)response.StatusCode);
                }
            });
        }

        /// <summary>
        /// Delete a ModuleConveyor
        /// </summary>
        /// <param name="id">A valid ID of the ModuleConveyor to delete</param>
        /// <returns>The deleted ModuleConveyor</returns>
        /// <remarks>
        /// For more information about the ModuleConveyor model, see the Topology Service documentation
        ///
        /// Example output:
        ///
        ///     DELETE /api/ModuleConveyors/{<paramref name="id"/>}
        ///     {
        ///         "moduleConveyorId": 0,
        ///         "moduleConveyorName": "unique-name",
        ///         "plcIpAddressString": "10.0.0.144",
        ///         "plcMacAddressString": "0011223344",
        ///         "resourceId": 0
        ///     }
        /// </remarks>
        /// <response code="503">No Topology Service</response>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(502)]
        [ProducesResponseType(503)]
        public async Task<IActionResult> DeleteModuleConveyor([FromRoute] int? id)
        {
            return await HandleRequest(async (httpClient) =>
            {
                using (var response = await httpClient.DeleteAsync($"{id}"))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        return this.OkJson(await response.Content.ReadAsStringAsync());
                    }
                    else return StatusCode((int)response.StatusCode);
                }
            });
        }
    }
}