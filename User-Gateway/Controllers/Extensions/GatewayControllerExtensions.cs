﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UserGateway.Controllers.Extensions
{
    public static class GatewayControllerExtensions
    {
        public static IActionResult OkJson(this Controller controller, string content)
        {
            return controller.Content(content, "application/json", Encoding.UTF8);
        }

        public static async Task<IActionResult> OkOrFailed(this Controller controller, HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                return controller.OkJson(await response.Content.ReadAsStringAsync());
            }
            else return controller.StatusCode((int)response.StatusCode);
        }

        public static async Task<IActionResult> CreatedAtActionOrFailedAsync(this Controller controller, HttpResponseMessage response, string actionName, string idName)
        {
            if (response.IsSuccessStatusCode)
            {
                var responseJson = JObject.Parse(await response.Content.ReadAsStringAsync());
                return controller.CreatedAtAction(actionName, new { id = responseJson[idName] }, responseJson);
            }
            else return controller.StatusCode((int)response.StatusCode);
        }
    }
}
