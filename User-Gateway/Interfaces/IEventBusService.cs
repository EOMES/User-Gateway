﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace UserGateway.Interfaces
{
    public interface IEventBusService
    {
        Task AddClientAsync(WebSocket webSocket);
    }
}
