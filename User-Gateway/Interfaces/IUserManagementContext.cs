﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserGateway.Models;

namespace UserGateway.Interfaces
{
    public interface IUserManagementContext
    {
        UriTuple Get();
        void Set(string hostname, ushort port, string sharedSecret);
    }
}
