﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserGateway.Models;

namespace UserGateway.Interfaces
{
    public interface IServiceRepository
    {
        IEnumerable<Service> All { get; }

        bool ServiceTypeExists(int serviceTypeId);
        bool ServiceExists(string serviceName);
        bool ServiceExists(Guid uuid);
        Service FindActiveByTypeId(int serviceTypeId);
        Service FindByName(string serviceName);
        Service FindByUuid(Guid uuid);
        IEnumerable<Service> FindAllByTypeId(int serviceTypeId);
        bool Add(Service service);
        void SetActive(Guid serviceUuid);
        void Remove(Service service);
        void RemoveServiceWithUuid(Guid uuid);
    }
}
