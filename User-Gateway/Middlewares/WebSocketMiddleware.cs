﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using UserGateway.Interfaces;

namespace UserGateway.Middlewares
{
    public class WebSocketMiddleware
    {
        protected WebSocket WebSocket { get; set; }

        private readonly RequestDelegate Next;

        protected readonly IEventBusService EventBusService;

        public WebSocketMiddleware(RequestDelegate next, IEventBusService eventBusService)
        {
            Next = next;
            EventBusService = eventBusService;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.WebSockets.IsWebSocketRequest)
            {
                var token = context.Request.Headers["Authorization"];
                if (token != StringValues.Empty)
                {
                    var socket = await context.WebSockets.AcceptWebSocketAsync();
                    await EventBusService.AddClientAsync(socket);
                }
                else context.Abort();
            }
            else
            {
                await Next(context);
            }
        }
    }
}
