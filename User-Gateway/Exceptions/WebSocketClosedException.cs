﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace UserGateway.Exceptions
{
    public class WebSocketClosedException : Exception
    {
        public WebSocketClosedException()
        {
        }

        public WebSocketClosedException(string message) : base(message)
        {
        }

        public WebSocketClosedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WebSocketClosedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
