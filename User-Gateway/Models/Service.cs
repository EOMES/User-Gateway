﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UserGateway.Models
{
    public class Service : Oxmes.Core.ServiceUtilities.Models.Service
    {
        [Key]
        public override Guid UUID { get => base.UUID; set => base.UUID = value; }

        [Required]
        public override string Name { get; set; }

        [Required]
        public override int ServiceType { get; set; }

        public override bool IsActive { get; set; }
    }
}
