﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserGateway.Data;
using UserGateway.Exceptions;
using UserGateway.Interfaces;
using UserGateway.Models;

namespace UserGateway.Services
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly UserGatewayContext Context;
        private readonly ILogger Logger;

        public IEnumerable<Service> All => Context.Services;

        public ServiceRepository(ILogger<ServiceRepository> logger, UserGatewayContext context)
        {
            Context = context;
            Logger = logger;
        }

        public bool Add(Service service)
        {
            if (!Context.Services.Any(s => s.Name == service.Name && s.UUID == service.UUID))
            {
                var firstOfItsType = !Context.Services.Any(s => s.ServiceType == service.ServiceType);
                service.IsActive = firstOfItsType;
                Context.Services.Add(service);
                Context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerable<Service> FindAllByTypeId(int serviceTypeId)
        {
            var result = All.Where(s => s.ServiceType == serviceTypeId);
            if (result.Count() > 0)
                return result;
            else
            {
                Logger.LogWarning($"Tried to find service with ID {serviceTypeId}, but no service with that ID was found");
                throw new ServiceNotFoundException($"No service with ID {serviceTypeId}");
            }
        }

        public Service FindByName(string serviceName)
        {
            try
            {
                return All.First(service => service.Name == serviceName);
            }
            catch (InvalidOperationException)
            {
                Logger.LogWarning($"Tried to find service with name {serviceName}, but no service with that name was found");
                throw new ServiceNotFoundException($"No service with name {serviceName}");
            }
        }

        public Service FindActiveByTypeId(int serviceTypeId)
        {
            var result = All.FirstOrDefault(s => s.ServiceType == serviceTypeId && s.IsActive);
            if (result != null)
                return result;
            else
            {
                Logger.LogWarning($"Tried to find active service with ID {serviceTypeId}, but no active service with that ID was found");
                throw new ServiceNotFoundException($"No active service with ID {serviceTypeId}");
            }
        }

        public Service FindByUuid(Guid uuid)
        {
            try
            {
                return All.First(service => service.UUID == uuid);
            }
            catch (InvalidOperationException)
            {
                Logger.LogWarning($"Tried to find service with UUID {uuid}, but no service with that UUID was found");
                throw new ServiceNotFoundException($"No service with UUID {uuid}");
            }
        }

        public void Remove(Service service)
        {
            var result = Context.Services.Remove(service);
            Context.SaveChanges();
        }

        public void RemoveServiceWithUuid(Guid uuid)
        {
            try
            {
                var service = Context.Services.First(s => s.UUID == uuid);
                Context.Services.Remove(service);
            }
            catch (InvalidOperationException)
            {

            }
        }

        public bool ServiceExists(string serviceName)
        {
            return All.Any(s => s.Name == serviceName);
        }

        public bool ServiceTypeExists(int serviceTypeId)
        {
            return All.Any(s => s.ServiceType == serviceTypeId);
        }

        public bool ServiceExists(Guid uuid)
        {
            return All.Any(s => s.UUID == uuid);
        }

        public void SetActive(Guid serviceUuid)
        {
            var service = FindByUuid(serviceUuid);
            var services = FindAllByTypeId(service.ServiceType);
            foreach (var s in services) s.IsActive = false;
            service.IsActive = true;
            Context.SaveChanges();
        }
    }
}
