﻿using Oxmes.Core.EventBusClient.Enums;
using Oxmes.Core.EventBusClient.Messages;
using Oxmes.Core.EventBusClient.Messages.Common;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UserGateway.Exceptions;
using UserGateway.Interfaces;
using Oxmes.Core.EventBusClient;
using System.Net.Http;
using Microsoft.Extensions.Configuration;

namespace UserGateway.Services
{
    public class EventBusService : IEventBusService
    {
        protected IList<EventBusClientWrapper> Clients { get; set; }

        private readonly IEventBusClient EventBusClient;
        private readonly IConfiguration Configuration;
        protected readonly ILogger Logger;

        public EventBusService(ILogger<EventBusService> logger, IEventBusClient eventBusClient, IConfiguration configuration)
        {
            EventBusClient = eventBusClient;
            Configuration = configuration;
            Logger = logger;

            Clients = new List<EventBusClientWrapper>();
        }

        public async Task AddClientAsync(WebSocket webSocket)
        {
            var message = await ReceiveMessageAsync(webSocket, CancellationToken.None);
            var messageType = message.Value<int>("MessageType");
            var severity = message.Value<int>("Severity");

            // Check if message is a valid registration message
            if (messageType != (int)CommonMessageTypes.Subscribe && severity != (int)Severity.Control)
            {
                Logger.LogWarning($"Received from first message. Expected <{(int)CommonMessageTypes.Subscribe},{(int)Severity.Control}> but received <{messageType},{severity}>");
                await webSocket.CloseAsync(WebSocketCloseStatus.InvalidMessageType,
                    $"Expected message type {(int)CommonMessageTypes.Subscribe} and severity {(int)Severity.Control}",
                    CancellationToken.None);
                return;
            }
            var subscribeMessage = message.ToObject<SubscribeMessage>();
            if (subscribeMessage.MessageUuid == Guid.Empty)
            {
                Logger.LogWarning($"Malformed message uuid. Received {message.Value<string>("MessageUuid")}");
                await webSocket.CloseAsync(WebSocketCloseStatus.InvalidMessageType,
                    "Malformed message uuid", CancellationToken.None);
                return;
            }
            if (subscribeMessage.OriginServiceUuid == Guid.Empty)
            {
                Logger.LogWarning($"Malformed service uuid. Received {message.Value<string>("OriginServiceUuid")}");
                await webSocket.CloseAsync(WebSocketCloseStatus.InvalidMessageType,
                    "Malformed service uuid", CancellationToken.None);
                return;
            }

            try
            {
                var ebcw = new EventBusClientWrapper { ClientUuid = subscribeMessage.OriginServiceUuid, WebSocket = webSocket };
                ebcw.AddSubscription(subscribeMessage.Payload);
                Clients.Add(ebcw);

                // We have to keep this open, otherwise the WebSocket framework will close the socket
                await ListenWebSocket(ebcw);
            }
            catch (ArgumentException ex)
            {
                Logger.LogWarning(ex, "Unable to add client");
                await webSocket.CloseAsync(WebSocketCloseStatus.InvalidPayloadData, ex.Message, CancellationToken.None);
            }

        }

        /// <summary>
        /// Listen for messages on a web socket in a loop
        /// </summary>
        /// <param name="webSocket">Web Socket to listen on</param>
        /// <returns></returns>
        protected async Task ListenWebSocket(EventBusClientWrapper ebcw)
        {
            try
            {
                while (ebcw.WebSocket.State == WebSocketState.Open)
                {
                    try
                    {
                        var message = await ReceiveMessageAsync(ebcw.WebSocket, CancellationToken.None);
                        var messageType = message.Value<int>("MessageType");
                        var severity = message.Value<int>("Severity");

                        Logger.LogDebug($"Received message: {message.ToString()}");


                        if (messageType == (int)CommonMessageTypes.Subscribe)
                        {
                            var sub = message.ToObject<SubscribeMessage>();
                            ebcw.AddSubscription(sub.Payload);
                        }
                        else
                        {
                            // Re-serialise message
                            var serialisedMessage = message.ToString(Formatting.None);
                            var buffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(serialisedMessage));

                            // Start looking for clients subscribed to this message type
                            // First iterate (in parallel for speed) over all registered clients
                            Clients.AsParallel().ForAll(async (client) =>
                            {
                                // Check whether this client subscripes to the message type
                                if (client.SubscribedEvents.TryGetValue(messageType, out (int min, int max) severityTuple))
                                {
                                    // Check that the client is subscribed to this severity
                                    if (severity.IsBetween(severityTuple.min, severityTuple.max))
                                    {
                                        try
                                        {
                                            await client.WebSocketTxSem.WaitAsync();
                                            if (client.WebSocket.State == WebSocketState.Open)
                                            {
                                                await client.WebSocket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
                                            }
                                        }
                                        finally
                                        {
                                            client.WebSocketTxSem.Release();
                                        }
                                    }
                                }
                            });
                        }

                    }
                    catch (WebSocketClosedException)
                    {
                        Logger.LogDebug("Received close message");
                        break;
                    }
                    catch (WebSocketException ex)
                    {
                        Logger.LogWarning("Unexpected error listening to websocket. Reason: " + ex.Message);
                        Logger.LogDebug(ex, "");
                        await EventBusClient.AnnounceShutdownAsync(ebcw.ClientUuid);
                        break;
                    }
                    catch (Exception ex)
                    {
                        Logger.LogWarning(ex, "Unexpected error listening to websocket.");
                    }
                }
                Logger.LogInformation($"Connection to service with UUID {ebcw.ClientUuid} closed");
                Clients.Remove(ebcw);
                // TODO: This fails because of scope. Use HttpClient and REST api to remove instead
                await RemoveServiceWithUuid(ebcw.ClientUuid);
            }
            catch (Exception ex)
            {
                Logger.LogWarning(ex, "Unexpected error listening to websocket.");
                Clients.Remove(ebcw);
                await RemoveServiceWithUuid(ebcw.ClientUuid);
            }
        }

        protected async Task<JObject> ReceiveMessageAsync(WebSocket webSocket, CancellationToken cancellationToken)
        {
            // Create 4 kb buffer
            var buffer = new ArraySegment<byte>(new byte[1024 << 2]);

            JObject json = null;
            bool isValid = false;
            do
            {
                var request = await webSocket.ReceiveAsync(buffer, cancellationToken);
                cancellationToken.ThrowIfCancellationRequested();
                if (request.MessageType == WebSocketMessageType.Text)
                {
                    var rawString = Encoding.UTF8.GetString(buffer.Array, index: buffer.Offset, count: buffer.Count);
                    json = JObject.Parse(rawString);
                    isValid = MessageValidator.ValidateMessage(json);
                }
                else if (request.MessageType == WebSocketMessageType.Close)
                {
                    await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed", CancellationToken.None);
                    throw new WebSocketClosedException();
                }
            } while (!isValid);
            return json;
        }

        private async Task RemoveServiceWithUuid(Guid Uuid)
        {
            try
            {
                var uri = new Uri(Configuration["urls"].Split(';')[0]);
                using (var httpClient = new HttpClient { BaseAddress = uri })
                {
                    using (var response = await httpClient.DeleteAsync($"api/service/{Uuid}"))
                    {
                        if (!response.IsSuccessStatusCode)
                        {
                            Logger.LogWarning($"Unable to remove service with UUID {Uuid}. Reason: {response.StatusCode}: {response.ReasonPhrase}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"Unable to remove service with UUID {Uuid}. Reason: {ex.Message}");
                Logger.LogDebug(ex, "");
            }
        }
    }

    public class EventBusClientWrapper
    {
        public Guid ClientUuid { get; set; }
        public WebSocket WebSocket { get; set; }
        public SemaphoreSlim WebSocketTxSem { get; } = new SemaphoreSlim(1);
        public IDictionary<int, ValueTuple<int, int>> SubscribedEvents { get; } = new Dictionary<int, ValueTuple<int, int>>();

        public void AddSubscription(SubscribeMessagePayload smp)
        {
            var UNDEFINED_MESSAGE = (int)CommonMessageTypes.Undefined;
            var MIN_SEVERITY = (int)Severity.Trace;
            var MAX_SEVERITY = (int)Severity.Control;

            // Validate message payload content
            if (smp.MessageType == UNDEFINED_MESSAGE)
            {
                throw new ArgumentException($"Invalid message type. Was {smp.MessageType}");
            }
            else if (smp.MinimumSeverity < MIN_SEVERITY)
            {
                throw new ArgumentException($"Invalid minimum severity. Was {smp.MinimumSeverity}, expected greater than or equal to {MIN_SEVERITY}");
            }
            else if (smp.MaximumSeverity > MAX_SEVERITY)
            {
                throw new ArgumentException($"Invalid maximum severity. Was {smp.MaximumSeverity}, expected less than or equal to {MAX_SEVERITY}");
            }

            (int min, int max) severityTuple = (smp.MinimumSeverity, smp.MaximumSeverity);

            // Fetch any existing subscription for this message type, and merge with this
            if (SubscribedEvents.TryGetValue(smp.MessageType, out var tuple))
            {
                tuple.Item1 = Math.Min(smp.MinimumSeverity, severityTuple.min);
                tuple.Item2 = Math.Max(smp.MaximumSeverity, severityTuple.max);
                severityTuple = tuple;
            }

            // Add or update subscription
            SubscribedEvents[smp.MessageType] = severityTuple;
        }
    }

    internal class InternalEventBusMessage : IEventBusMessage
    {
        public Guid MessageUuid { get; }

        public Guid OriginServiceUuid { get; }

        public Severity Severity { get; }

        public int MessageType { get; }

        public DateTime Time { get; }

        public object Payload { get; }
    }

    static class ExtensionMethods
    {
        internal static bool IsBetween(this int i, int min, int max)
        {
            return min <= i && i <= max;
        }
    }
}
