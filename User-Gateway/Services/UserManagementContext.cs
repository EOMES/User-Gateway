﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserGateway.Interfaces;
using UserGateway.Models;

namespace UserGateway.Services
{
    public class UserManagementContext : IUserManagementContext
    {
        protected string Host { get; set; }
        protected ushort Port { get; set; }

        private readonly ILogger Logger;

        public UserManagementContext(ILogger<UserManagementContext> logger)
        {
            Logger = logger;
        }

        public UriTuple Get()
        {
            return new UriTuple { Host = Host, Port = Port };
        }

        public void Set(string host, ushort port, string sharedSecret)
        {
            if (Host == null && Port == 0)
            {
                Host = host;
                Port = port;
            }
            else throw new InvalidOperationException("User Management Service already set");
        }
    }
}
