﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using UserGateway.Interfaces;
using UserGateway.Services;
using Oxmes.Core.EventBusClient;
using Oxmes.Core.ServiceUtilities;
using UserGateway.Middlewares;
using Oxmes.Core.EventBusClient.Exceptions;
using UserGateway.Data;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System.IO;

namespace UserGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            Logger = logger;
        }

        public IConfiguration Configuration { get; }

        private readonly ILogger Logger;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<UserGatewayContext>(options => options.UseInMemoryDatabase("UserGateway"));

            services.AddTransient<IServiceRepository, ServiceRepository>();
            services.AddSingleton<IEventBusService, EventBusService>();
            services.AddSingleton<IServiceClient, ServiceClient>();
            services.AddSingleton<IEventBusClient, EventBusClient>();
            services.AddSingleton<IUserManagementContext, UserManagementContext>();

            services.AddMvc();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "User Gateway API", Version = "v1" });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IApplicationLifetime lifetime, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            lifetime.ApplicationStarted.Register(ApplicationStarted, app.ApplicationServices);
            lifetime.ApplicationStopping.Register(ApplicationStopping, app.ApplicationServices);

            app.UseWebSockets();
            app.UseMiddleware<WebSocketMiddleware>();

            app.UseMvc();

            // Serve Swagger Json
            app.UseSwagger();

            // Enable Swagger UI
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "User Gateway API v1");
            });
        }

        private async void ApplicationStarted(object serviceProvider)
        {
            if (serviceProvider is IServiceProvider applicationServices)
            {
                var gatewayAddress = new Uri(Configuration["urls"].Split(';')[0]);

                // Connect as service
                var serviceClient = applicationServices.GetService<IServiceClient>();
                serviceClient.GatewayHost = "localhost";
                serviceClient.GatewayPort = (ushort)gatewayAddress.Port;
                serviceClient.UseHttps = gatewayAddress.Scheme == "https";
                serviceClient.ApiKey = "...";
                serviceClient.ServiceName = "user-gateway";
                serviceClient.ServiceDisplayName = "User Gateway";
                serviceClient.ServiceType = (int)Oxmes.Core.ServiceTypes.ServiceType.UserGateway;
                await serviceClient.ConnectAsync();

                var eventBusClient = applicationServices.GetService<IEventBusClient>();
                eventBusClient.Host = "localhost";
                eventBusClient.Port = (ushort)gatewayAddress.Port;
                eventBusClient.AuthorizationToken = "";
                eventBusClient.ServiceUuid = serviceClient.ServiceUuid;
                await eventBusClient.ConnectAsync();
                await eventBusClient.SubscribeAsync(1000);
                await eventBusClient.SubscribeAsync(1000);
            }
        }

        private void ApplicationStopping(object serviceProvider)
        {
            if (serviceProvider is IServiceProvider applicationServices)
            {
                var eventBusClient = applicationServices.GetService<IEventBusClient>();

                try
                {
                    eventBusClient.CloseAsync().Wait();
                }
                catch (ConnectionLostException)
                {
                }
            }
        }
    }
}